package com.epam.lab.rto.demo;

import com.epam.lab.rto.dto.RailCarriageType;
import com.epam.lab.rto.dto.Request;
import com.epam.lab.rto.dto.User;
import com.epam.lab.rto.dto.UserRole;
import com.epam.lab.rto.service.IRequestService;
import com.epam.lab.rto.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

@Service
public class DemoService implements IDemoService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IRequestService requestService;

    public void execute() {

        fillUsers();

        System.out.println("\n Показать всех пользователей:");
        showAll(userService.getAll());

        String str = "Иван иванов";
        System.out.printf("\n Найти пользователей по имени: %s\n", str);
        showAll(userService.findUsersByName(str));

        User user = new User("Созонов", "Антон", "Сергеевич", LocalDate.of(1993, 4, 6), true, UserRole.USER);

        System.out.printf("\n Добавить нового пользователя: %s\n", user);
        userService.addUser(user);
        userService.addUser(user);
        showAll(userService.getAll());

        fillRequests();

        System.out.println("\n Показать все запросы пользователей:");
        showAll(requestService.getAll());

        user = userService.getUserById(4);
        System.out.printf("\n Найти запросы пользователя: %s\n", user);
        showAll(requestService.findByUser(user));

        LocalDate start = LocalDate.of (2018, 12, 1);
        LocalDate end = LocalDate.of (2019, 1, 1);
        System.out.printf("\n Найти запросы c датой отправки от %s до %s\n", start, end);
        showAll(requestService.findByDateRange(start, end));

    }

    private void showAll (List <?> list) {
        list.forEach(System.out::println);
    }

    private void fillUsers() {

        Arrays.stream(DemoUsers.USERS).forEach(userService::addUser);

    }

    private void fillRequests(){

       requestService.addRequest(new Request(userService.getUserById(2),
                "Москва",
                "Ижевск",
                LocalDate.of(2018, 12, 22),
                LocalTime.of(12, 0),
                RailCarriageType.COUPE));
        requestService.addRequest(new Request(userService.getUserById(4),
                "Владивосток",
                "Челябинск",
                LocalDate.of(2018, 12, 31),
                LocalTime.of(23, 15),
                RailCarriageType.COUPE));
        requestService.addRequest(new Request(userService.getUserById(1),
                "Казань",
                "Пермь",
                LocalDate.of(2019, 1, 5),
                LocalTime.of(13, 40),
                RailCarriageType.PLATZKART));
        requestService.addRequest(new Request(userService.getUserById(4),
                "Ижевск",
                "Москва",
                LocalDate.of(2019, 1, 4),
                LocalTime.of(14, 0),
                RailCarriageType.COUPE));

    }

}
