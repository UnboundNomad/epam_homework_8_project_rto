package com.epam.lab.rto.demo;

import com.epam.lab.rto.dto.User;
import com.epam.lab.rto.dto.UserRole;

import java.time.LocalDate;

public class DemoUsers {

    public static User[] USERS = new User[]{
            new User("Иванов", "Иван", "Иванович", LocalDate.of(1985, 4, 25), true, UserRole.USER),
            new User("Валентинова", "Валентина", "Валентиновна", LocalDate.of(1992, 12, 1), false, UserRole.USER),
            new User("Петров", "Пётр", "Петрович", LocalDate.of(1966, 5, 6), true, UserRole.USER),
            new User("Васильев", "Василий", "Васильевич", LocalDate.of(1991, 5, 22), true, UserRole.USER)
    };

}
