package com.epam.lab.rto;

import com.epam.lab.rto.demo.IDemoService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RailwayTicketOfficeApplication {

	public static void main(String[] args) {

		SpringApplication.run(RailwayTicketOfficeApplication.class, args).getBean(IDemoService.class).execute();

	}

}

