package com.epam.lab.rto.service;

import com.epam.lab.rto.dto.Request;
import com.epam.lab.rto.dto.User;
import com.epam.lab.rto.repository.IRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class RequestService implements IRequestService {

    @Autowired
    private IRequestRepository requestRepository;

    @Override
    public void addRequest(Request request) {
        if (request != null) {
            Request lastRequest = requestRepository.getLastRequest();

            if (lastRequest == null) {
                requestRepository.addRequest(new Request(1, request));
            } else {
                requestRepository.addRequest(new Request(lastRequest.getId() + 1, request));
            }
        }
    }

    @Override
    public void deleteRequest(long id) {
        if (id > 0) {
            Request request = getRequestById(id);
            if (request != null) {
                requestRepository.deleteRequest(request);
            }
        }
    }

    @Override
    public void updateRequest(Request updateRequest) {
        if (updateRequest != null) {
            Request oldRequest = getRequestById(updateRequest.getId());
            if (oldRequest != null) {
                requestRepository.updateRequest(oldRequest, updateRequest);
            }
        }
    }

    @Override
    public List<Request> getAll() {
        return requestRepository.getAll();
    }

    @Override
    public Request getRequestById(long id) {
        if (id > 0) return requestRepository.getRequestById(id);
        return null;
    }

    @Override
    public List<Request> findByUser(User user) {
        if (user != null) {
            return requestRepository.findByUser(user);
        }
        return null;
    }

    @Override
    public List<Request> findByDateRange(LocalDate start, LocalDate end) {
        if (start != null && end != null) {
            return start.isBefore(end) ? requestRepository.findByDateRange(start, end) : requestRepository.findByDateRange(end, start);
        }
    return null;
    }
}
