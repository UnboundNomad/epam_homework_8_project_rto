package com.epam.lab.rto.service;

import com.epam.lab.rto.dto.Request;
import com.epam.lab.rto.dto.User;

import java.time.LocalDate;
import java.util.List;

public interface IRequestService {

    void addRequest (Request request);

    void deleteRequest(long id);

    void updateRequest (Request updateRequest);

    List<Request> getAll();

    Request getRequestById(long id);

    List<Request> findByUser(User user);

    List<Request> findByDateRange (LocalDate start, LocalDate end);

}
