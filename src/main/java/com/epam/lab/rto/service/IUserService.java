package com.epam.lab.rto.service;

import com.epam.lab.rto.dto.User;

import java.util.List;

public interface IUserService {

    void addUser (User user);

    void deleteUser (long id);

    void updateUser (User updateUser);

    User getUserById(long id);

    List<User> getAll();

    List<User> findUsersByName(String name);
}
