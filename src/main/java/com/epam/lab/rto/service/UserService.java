package com.epam.lab.rto.service;

import com.epam.lab.rto.dto.User;
import com.epam.lab.rto.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    private static long ID_COUNTER = 1L;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public void addUser(User user) {
        if (user != null && !isUserExists(user)) userRepository.addUser(new User(ID_COUNTER++, user));
    }

    @Override
    public void deleteUser(long id) {
        User user = getUserById(id);
        if (user != null) userRepository.deleteUser(user);
    }

    @Override
    public void updateUser(User updateUser) {
        if (updateUser != null) {
            User oldUser = getUserById(updateUser.getId());
            if (oldUser != null) {
                userRepository.updateUser(oldUser, updateUser);
            }
        }
    }

    @Override
    public User getUserById(long id) {
        if (id > 0) {
            return userRepository.getUserById(id);
        }
        return null;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<User> findUsersByName(String search) {
        if (search != null) {
            return userRepository.findUsersByName(search);
        }
        return null;
    }

    private boolean isUserExists(User user) {
        return userRepository.getAll().stream().anyMatch(e -> e.equals(user));
    }
}
