package com.epam.lab.rto.dto;

public enum UserRole {

    GUEST, USER, ADMIN

}
