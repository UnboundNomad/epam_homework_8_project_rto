package com.epam.lab.rto.dto;

import java.math.BigDecimal;

public enum RailCarriageType {

    COUPE(36, 1.3),
    PLATZKART(54, 1.0),
    COACH(63, 0.5);

    RailCarriageType(int places, Double priceFactor) {

        this.places = places;
        this.priceFactor = new BigDecimal(priceFactor);

    }

    private int places;
    private BigDecimal priceFactor;

    public int getPlaces() {
        return places;
    }

    public BigDecimal getPriceFactor() {
        return priceFactor;
    }
}
