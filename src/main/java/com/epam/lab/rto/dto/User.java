package com.epam.lab.rto.dto;

import java.time.LocalDate;
import java.util.Objects;

public class User {

    private long id;

    private String surname;

    private String name;

    private String patronymic;

    private LocalDate birthDate;

    private boolean gender;

    private UserRole role;

    public long getId() {
        return id;
    }

    public User(String surname, String name, String patronymic, LocalDate birthDate, boolean gender, UserRole role) {
        this.id = 0L;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.birthDate = birthDate;
        this.gender = gender;
        this.role = role;
    }

    public User(long id, User user) {
        this.id = id;
        this.surname = user.surname;
        this.name = user.name;
        this.patronymic = user.patronymic;
        this.birthDate = user.birthDate;
        this.gender = user.gender;
        this.role = user.role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getFullName() {return String.format("%s %s %s", this.surname, this.name, this.patronymic); }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public boolean getGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }


    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return String.format("id: %s; Имя: %s; Дата рождения: %s.",
                this.id,
                this.getFullName(),
                this.birthDate.toString()
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return  getGender() == user.getGender() &&
                Objects.equals(getSurname(), user.getSurname()) &&
                Objects.equals(getName(), user.getName()) &&
                Objects.equals(getPatronymic(), user.getPatronymic()) &&
                Objects.equals(getBirthDate(), user.getBirthDate()) &&
                getRole() == user.getRole();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSurname(), getName(), getPatronymic(), getBirthDate(), getGender(), getRole());
    }
}
