package com.epam.lab.rto.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class Request {

    private long id;

    private User user;

    private String departureStation;

    private String destinationStation;

    private LocalDate departureDate;

    private LocalTime departureTime;

    private RailCarriageType railCarriageType;


    public Request(User user, String departureStation, String destinationStation, LocalDate departureDate, LocalTime departureTime, RailCarriageType railCarriageType) {
        this.id = 0;
        this.user = user;
        this.departureStation = departureStation;
        this.destinationStation = destinationStation;
        this.departureDate = departureDate;
        this.departureTime = departureTime;
        this.railCarriageType = railCarriageType;
    }

    public Request(long id, Request request) {
        this.id = id;
        this.user = request.user;
        this.departureStation = request.departureStation;
        this.destinationStation = request.destinationStation;
        this.departureDate = request.departureDate;
        this.departureTime = request.departureTime;
        this.railCarriageType = request.railCarriageType;
    }


    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDepartureStation() {
        return departureStation;
    }

    public void setDepartureStation(String departureStation) {
        this.departureStation = departureStation;
    }

    public String getDestinationStation() {
        return destinationStation;
    }

    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    public RailCarriageType getRailCarriageType() {
        return railCarriageType;
    }

    public void setRailCarriageType(RailCarriageType railCarriageType) {
        this.railCarriageType = railCarriageType;
    }

    @Override
    public String toString() {
        return String.format("id: %s; Имя: \"%s\"; Маршрут: \"%s-%s\"; Дата-время: \"%s %s\"",
                this.id,
                this.user.getFullName(),
                this.departureStation,
                this.destinationStation,
                this.departureDate,
                this.departureTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(getUser(), request.getUser()) &&
                Objects.equals(getDepartureStation(), request.getDepartureStation()) &&
                Objects.equals(getDestinationStation(), request.getDestinationStation()) &&
                Objects.equals(getDepartureDate(), request.getDepartureDate()) &&
                Objects.equals(getDepartureTime(), request.getDepartureTime()) &&
                getRailCarriageType() == request.getRailCarriageType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), getDepartureStation(), getDestinationStation(), getDepartureDate(), getDepartureTime(), getRailCarriageType());
    }
}
