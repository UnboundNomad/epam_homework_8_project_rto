package com.epam.lab.rto.repository;

import com.epam.lab.rto.dto.Request;
import com.epam.lab.rto.dto.User;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class RequestRepository implements IRequestRepository {

    private SortedSet<Request> requests = new TreeSet<>(Comparator.comparingLong(Request::getId));

    @Override
    public void addRequest(Request request) {
        requests.add(request);
    }

    @Override
    public void deleteRequest(Request request) {
        requests.remove(request);
    }

    @Override
    public void updateRequest(Request oldRequest, Request newRequest) {
        requests.remove(oldRequest);
        requests.add(newRequest);
    }

    @Override
    public List<Request> getAll() {
        return new ArrayList<>(requests);
    }

    @Override
    public Request getRequestById(long id) {
        return requests.stream().filter(e -> e.getId() == id).findAny().orElse(null);
    }

    @Override
    public Request getLastRequest() {
        if (requests.isEmpty()) return null;
        return requests.last();
    }


    @Override
    public List<Request> findByUser(User user) {
        return requests.stream().filter(e -> e.getUser().equals(user)).collect(Collectors.toList());
    }

    @Override
    public List<Request> findByDateRange(final LocalDate start, final LocalDate end) {
        return requests.stream().filter(e -> e.getDepartureDate().isAfter(start)).filter(e -> e.getDepartureDate().isBefore(end)).collect(Collectors.toList());
    }

}
