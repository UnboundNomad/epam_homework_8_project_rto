package com.epam.lab.rto.repository;

import com.epam.lab.rto.dto.User;

import java.util.List;

public interface IUserRepository {

    void addUser (User user);

    void deleteUser (User user);

    void updateUser (User oldUser, User newUser);

    User getUserById(long id);

    List<User> getAll();

    List<User> findUsersByName(String search);

}
