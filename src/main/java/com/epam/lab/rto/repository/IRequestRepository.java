package com.epam.lab.rto.repository;

import com.epam.lab.rto.dto.Request;
import com.epam.lab.rto.dto.User;

import java.time.LocalDate;
import java.util.List;

public interface IRequestRepository {

    void addRequest (Request request);

    void deleteRequest(Request request);

    void updateRequest (Request oldRequest, Request newRequest);

    List<Request> getAll();

    Request getRequestById(long id);

    Request getLastRequest ();

    List<Request> findByUser(User user);

    List<Request> findByDateRange (LocalDate start, LocalDate end);

}
