package com.epam.lab.rto.repository;

import com.epam.lab.rto.dto.User;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;


@Repository
public class UserRepository implements IUserRepository {

    private SortedSet<User> users = new TreeSet<>(Comparator.comparingLong(User::getId));

    @Override
    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public void deleteUser(User user) {
        users.remove(user);
    }

    @Override
    public void updateUser(User oldUser, User newUser) {
        users.remove(oldUser);
        users.add(newUser);
    }

    @Override
    public User getUserById(long id) {
        return users.stream().filter(e -> e.getId() == id).findAny().orElse(null);
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(users);
    }

    @Override
    public List<User> findUsersByName(String search) {
        return users.stream().filter(user -> isStringContainsWords(user.getFullName(), search)).collect(Collectors.toList());
    }

    private boolean isStringContainsWords(String string, String words) {
        for (String word : words.split(" ")) {
            if (!string.toLowerCase().contains(word.toLowerCase())) return false;
        }

        return true;
    }

}
